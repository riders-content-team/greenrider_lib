#!/usr/bin/env python2
import rospy
from geometry_msgs.msg import Point, PoseStamped
from greenrider_description.srv import CameraService
from gazebo_msgs.srv import GetModelState
from std_msgs.msg import Bool, Float64
import requests
import os
import numpy as np
import cv2 as cv
from sensor_msgs.msg import Image, CompressedImage
from cv_bridge import CvBridge, CvBridgeError
from tf.transformations import quaternion_from_euler

class RobotControl:

    def __init__(self, robot_name):
        self.robot_name = robot_name

        rospy.init_node('control', anonymous=True)
        self.pose_pub = rospy.Publisher('/' + self.robot_name + '/set_pose', Point, queue_size=1)
        self.pose_msg = Point()

        self.belt_pub = rospy.Publisher('/belt_state', Bool, queue_size=1)
        self.belt_msg = Bool()

        self.grip_pub = rospy.Publisher('/' + self.robot_name + '/grip_state', Bool, queue_size=1)
        self.grip_msg = Bool()

        self.new_object_pub = rospy.Publisher('/' + self.robot_name + '/spawn_new_object', Bool, queue_size=1)
        self.new_obj_msg = Bool()

        self.image = self._Image()
        self.init_sensor_services()

        #arm
        self.pub = rospy.Publisher('/iiwa/command/CartesianPose', PoseStamped, queue_size=10)
        self.pos = PoseStamped() 

        self.grabber_left_pub = rospy.Publisher('/iiwa/grabber_left_joint/set_speed', Float64, queue_size=10)
        self.left_gripper_msg = Float64()

        self.grabber_right_pub = rospy.Publisher('/iiwa/grabber_right_joint/set_speed', Float64, queue_size=10)
        self.right_gripper_msg = Float64()

        self.gripper_state = Bool()


    def init_sensor_services(self):
        try:
            rospy.wait_for_service("/greenrider/get_image", 1.0)
            self.image_data_service = rospy.ServiceProxy('/greenrider/get_image', CameraService)
            resp = self.image_data_service()

            self.image.height = resp.height
            self.image.width = resp.width

            return True
        except (rospy.ServiceException, rospy.ROSException):
            print("no camera sensor")
            return False 
    #arm
    def set_arm_pose(self,posX,posY,posZ,roll,pitch,yaw):        
        self.pos.header.seq = 1
        self.pos.header.stamp = rospy.Time.now()
        self.pos.header.frame_id = "iiwa_link_0"

        self.pos.pose.position.x = posX
        self.pos.pose.position.y = posY
        self.pos.pose.position.z = posZ

        q = quaternion_from_euler(roll,pitch,yaw)

        self.pos.pose.orientation.x = q[0]
        self.pos.pose.orientation.y = q[1]
        self.pos.pose.orientation.z = q[2]
        self.pos.pose.orientation.w = q[3]      
        self.pub.publish(self.pos)     
        rospy.sleep(2)
    
    def get_arm_pose(self):
        self.sub = rospy.Subscriber('/iiwa/state/CartesianPose', PoseStamped, self.subsFonk)
        rospy.spin()
    
    def subsFonk(self,data):
        print(data)     
            
    def gripper(self, state):
        if state == True:
            self.left_gripper_msg.data = 0.05
            self.right_gripper_msg.data = -0.05
        else:
            self.left_gripper_msg.data = -0.05
            self.right_gripper_msg.data = 0.05

        self.grabber_left_pub.publish(self.left_gripper_msg)
        self.grabber_right_pub.publish(self.right_gripper_msg)
        self.grip_msg.data = state
        self.grip_pub.publish(self.grip_msg)

    #robot
    def new_object(self, state):
        self.new_obj_msg.data = state
        self.new_object_pub.publish(self.new_obj_msg)

    def start_conveyor(self, state):
        self.belt_msg.data = state
        self.belt_pub.publish(self.belt_msg)
    
    def grip(self, state):
        self.grip_msg.data = state
        self.grip_pub.publish(self.grip_msg)

    def set_position(self,x,y,z):
        self.pose_msg.x = x
        self.pose_msg.y = y
        self.pose_msg.z = z
        self.pose_pub.publish(self.pose_msg)
    
    def image_data(self):
        resp = self.image_data_service()
        self.image.data = []
        for pixel in resp.data:
            self.image.data.append(ord(pixel))
        #print(self.image.data)
        self.achieve(154)
        return self.image


    def is_ok(self):
        if not rospy.is_shutdown():
            return True
        else:
            return False

    def achieve(self, achievement_id):
        # get parameters from environment
        host = os.environ.get('RIDERS_HOST', None)
        project_id = os.environ.get('RIDERS_PROJECT_ID', None)
        token = os.environ.get('RIDERS_AUTH_TOKEN', None)

        # generate request url
        achievement_url = '%s/api/v1/project/%s/achievements/%s/achieve/' % (host, project_id, achievement_id)

        # send request
        rqst = requests.post(achievement_url, headers={
            'Authorization': 'Token %s' % token,
            'Content-Type': 'application/json',
        })


    class _Image:
        def __init__(self):
            self.height = 0
            self.width = 0
            self.data = []
