#!/usr/bin/env python2
from greenrider_lib.robot_control import RobotControl
import time

robot = RobotControl("greenrider")


time.sleep(5)
while robot.is_ok():
    robot.image_data()
    robot.set_position(0,0,5)
    robot.new_object(True)
    robot.start_conveyor(True)
    time.sleep(15)
    robot.start_conveyor(False)
    time.sleep(3)
    robot.grip(True)
    robot.set_position(0,0,0)
    time.sleep(5)
    robot.set_position(0,0,5)
    time.sleep(5)
    robot.set_position(-1.31,1.97,0.49)
    time.sleep(4)
    robot.grip(False)
    time.sleep(10)


